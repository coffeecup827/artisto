import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom'
import 'materialize-css/dist/css/materialize.min.css';
import 'materialize-css/dist/js/materialize.min';
import { createStore, applyMiddleware, compose } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import reducer from './reducers/reducer';
import { Route,Switch } from 'react-router-dom'

import './css/main.css';

import Home from './screens/home';
import Artist from './screens/artist';
import ArtistList from './screens/artistList';
import Navbar from './components/navbar';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(reducer,composeEnhancers(applyMiddleware(thunk)));

ReactDOM.render(
  
  <Provider store={store}>
    <BrowserRouter>
    <div>

      <Navbar />

      <div>

        <Route exact path="/" render={() => (
            <Home />
          )}>
        </Route>

        <Switch>

          <Route exact path="/artists" render={() => (
              <ArtistList />
            )}>
          </Route>

          <Route path="/artists/:artist" render={() => (
              <Artist />
            )}>
          </Route>

        </Switch>

      </div>
    </div>
    </BrowserRouter>
  </Provider>

  , document.getElementById('root')
);
