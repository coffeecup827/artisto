import React, { Component } from 'react'
import { connect } from 'react-redux'
import RequestParam from '../helpers/requestParam';
import {UpdateTrending} from '../actions/trendingActions';
import axios from 'axios';
import Loader from '../components/loader';
import ErrorInfo from '../components/errorInfo';
import AlbumnsGrid from '../components/grids/albumnsGrid';
import SinglesGrid from '../components/grids/singlesGrid';
import PropTypes from 'prop-types';

/**
 * Shows list of trending albums and single tracks.
 * click on either of album or single shows info about its album
 */

export class Home extends Component {
  
  constructor(props){
    super(props);
    this.state = {
      showError : false,
      showLoader : false,
      cancelSource : null
    }
  }

  componentDidMount(){
    if (this.props.trendingAlbumns.length === 0 && this.props.tredingSingles.length === 0) {

      let cancelSource = axios.CancelToken.source();

      this.setState({
        showLoader : true,
        cancelSource
      });

      let requestParam = new RequestParam();
      requestParam.RequestData = {
        cancelToken : cancelSource.token
      };
      requestParam.RequestFailedAction = this.showError;
      requestParam.RequestSuccessAction = this.hideLoader;

      this.props.UpdateTrending(requestParam);
    }
  }

  componentWillUnmount(){
    /**
     * cancel peding requests
     */
    if(this.state.cancelSource != null)
      this.state.cancelSource.cancel("Cancelled");
  }

  showError = () => {
    this.setState({
      showError : true,
      showLoader : false
    })
  }

  hideLoader = () => {
    this.setState({
      showError : false,
      showLoader : false
    })
  }

  render() {
    return (
      <div>
        {
          this.state.showLoader && 
          <Loader />
        }
        {
          this.state.showError && 
          <ErrorInfo />
        }

        <AlbumnsGrid title="Trending Albums" albums={this.props.trendingAlbumns} />
        
        <br />

        <SinglesGrid title="Trending Singles" singles={this.props.tredingSingles} />

      </div>
    )
  }
}

const mapStateToProps = ({trendingReducer}) => ({
  trendingAlbumns : trendingReducer.tredingAlbumns,
  tredingSingles : trendingReducer.tredingSingles
})

const mapDispatchToProps = (dispatch) => ({
  UpdateTrending : (data) => dispatch(UpdateTrending(data))
})

Home.propTypes = {
  trendingAlbumns: PropTypes.array,
  tredingSingles : PropTypes.array,
  UpdateTrending : PropTypes.func
};

export default connect(mapStateToProps, mapDispatchToProps)(Home)
