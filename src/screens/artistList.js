import React, { Component } from 'react';
import {withRouter} from 'react-router';
import {Link} from 'react-router-dom';
import queryString from 'query-string';
import axios from 'axios';
import { connect } from 'react-redux';
import Loader from '../components/loader';
import ErrorInfo from '../components/errorInfo';
import Paginator from '../components/paginator';
import {FetchArtistSearchData} from '../actions/artistActions';
import RequestParam from '../helpers/requestParam';
import PropTypes from 'prop-types';

/**
 * Shows list of result of artist search
 * click on a artist info opens us the artist info page
 */

class ArtistList extends Component {

  constructor(props){
    super(props);
    this.state = {
      showError : false,
      showLoader : false,
      cancelSource : null,
      page : 1,
      showList : true
    }
  }

  componentDidMount() {
    this.setState({
      showList : false
    },() => {
      this.searchArtist();
    })
  }

  componentDidUpdate(prevProps){
    if (queryString.parse(this.props.location.search).search !== queryString.parse(prevProps.location.search).search) {
      this.setState({
        showList : false
      },() => {
        this.searchArtist();
      })
    }
  }

  searchArtist = () => {
    /**
     * get artist name to search from URL params
     */
    let query = queryString.parse(this.props.location.search);
    /**
     * cancel any pending requests
     */
    if (this.state.cancelSource!==null) {
      this.state.cancelSource.cancel("Cancelled");
    }

    let cancelSource = axios.CancelToken.source();

    this.setState({
      showLoader : true,
      cancelSource
    });

    let requestParam = new RequestParam();
      requestParam.RequestData = {
        cancelToken : cancelSource.token,
        artist : query.search
      };
    requestParam.RequestFailedAction = this.showError;
    requestParam.RequestSuccessAction = this.hideLoader;

    this.props.FetchArtistSearchData(requestParam);
  }

  componentWillUnmount(){
    /**
     * cancel peding requests
     */
    if(this.state.cancelSource != null)
      this.state.cancelSource.cancel("Cancelled");
  }

  showError = () => {
    this.setState({
      showError : true,
      showLoader : false
    })
  }

  hideLoader = () => {
    this.setState({
      showError : false,
      showLoader : false,
      showList : true
    })
  }

  gotoPage = (page) => {
    this.setState({
      page
    })
  }

  render() {

    let notAvailableString  = "Not Available";

    return (
      <div>

        {
          this.state.showLoader && 
          <Loader />
        }
        {
          this.state.showError && 
          <ErrorInfo />
        }

        { this.state.showList && this.props.artistList !== null && this.props.artistList[this.state.page].length > 0 && 
        <div>
          <div className="row">
            <h4 className="grid-header">Artists</h4>

            <div className="padding-10">
            {
              this.props.artistList[this.state.page].map((artist) => (
                <Link to={"/artists/" + artist.idArtist} key={artist.idArtist}>
                  <div className="col s12 m4 artist-card">
                    <div className="row hoverable hover z-depth-2">
                      <div className="col s12 m6 artist-card-image" 
                        style={{backgroundImage : `url(${(artist.strArtistThumb !== null && artist.strArtistThumb !== "") ? artist.strArtistThumb : require("../assets/music.png")})`}}
                      >
                        
                      </div>
                      <div className="col s12 m6">
                        <h5>{artist.strArtist}</h5>
                        <p>{artist.strBiographyEN.substring(0,100)}...</p>
                        <h6>Genre : {artist.strGenre !== "" ? artist.strGenre : notAvailableString}</h6>
                        <h6>Country : {artist.strCountry !== "" ? artist.strCountry : notAvailableString}</h6>
                      </div>
                    </div>
                  </div>
                </Link>
              ))
            }
            </div>
            
          </div>

          <Paginator list={Object.keys(this.props.artistList)} gotoPage={this.gotoPage}/>
        
        </div>
      }

      {
        this.state.showList && this.props.artistList === null&& 
        <ErrorInfo message="No Artists Found" />
      }

      </div>
    )
  }
}

const mapStateToProps = ({artistReducer}) => ({
  artistList : artistReducer.artistList
})

const mapDispatchToProps = (dispatch) => ({
  FetchArtistSearchData : (data) => dispatch(FetchArtistSearchData(data))
})

ArtistList.propTypes = {
  artistList: PropTypes.object,
  FetchArtistSearchData : PropTypes.func
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ArtistList));