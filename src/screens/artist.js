/*global M*/
import React, { Component } from 'react';
import {withRouter} from 'react-router'
import {connect} from 'react-redux';
import axios from 'axios';
import Loader from '../components/loader';
import ErrorInfo from '../components/errorInfo';
import Paginator from '../components/paginator';
import {FetchArtistData} from '../actions/artistActions';
import RequestParam from '../helpers/requestParam';
import AlbumnsGrid from '../components/grids/albumnsGrid';
import PropTypes from 'prop-types';

/**
 * Shows the info about the selected artist and his his albums
 * clicking on album shows info about the album
 */

class Artist extends Component {

  constructor(props){
    super(props);
    this.state = {
      showError : false,
      showLoader : false,
      cancelSource : null,
      albumPage : 1,
      showList : false,
      artistBioModal : "artistBioModal"
    }
  }

  componentDidMount() {
    this.setState({
      showList : false
    },() => {
      this.fetchArtist();
    });
    M.Modal.init(document.getElementById(this.state.artistBioModal));
  }

  fetchArtist = () => {
    /**
     * get selected artistId from url
     */
    let artistId = this.props.match.params.artist;
    /**
     * cancel any pending requests
     */
    if (this.state.cancelSource!==null) {
      this.state.cancelSource.cancel("Cancelled");
    }

    let cancelSource = axios.CancelToken.source();

    this.setState({
      showLoader : true,
      cancelSource
    });

    let requestParam = new RequestParam();
      requestParam.RequestData = {
        cancelToken : cancelSource.token,
        artistId : artistId
      };
    requestParam.RequestFailedAction = this.showError;
    requestParam.RequestSuccessAction = this.hideLoader;

    this.props.FetchArtistData(requestParam);
  }

  componentWillUnmount(){
    /**
     * cancel peding requests and close artistBioModal
     */
    if(this.state.cancelSource != null)
      this.state.cancelSource.cancel("Cancelled");
    M.Modal.getInstance(document.getElementById(this.state.artistBioModal)).close();
  }

  showError = () => {
    this.setState({
      showError : true,
      showLoader : false
    })
  }

  hideLoader = () => {
    this.setState({
      showError : false,
      showLoader : false,
      showList : true
    })
  }

  gotoPage = (albumPage) => {
    this.setState({
      albumPage
    })
  }

  render() {

    const artist = this.props.artist;
    let notAvailableString  = "Not Available";

    return (
      <div className="artist-profile-container">

        {
          this.state.showLoader && 
          <Loader />
        }
        {
          this.state.showError && 
          <ErrorInfo />
        }

        {
          this.state.showList && 
          <div>
            <div id="artist-info-container" className="row">
              <div className="col s12 m3 push-m1 z-depth-3 artist-image" style={{backgroundImage : `url(${artist.strArtistThumb !== null ? artist.strArtistThumb : require("../assets/music.png")})`}}>

              </div>
              <div className="col show-on-large push-m1 gap">

              </div>
              <div className="col s12 m5 push-m1">
                <h5>{artist.strArtist}</h5>
                <p>
                  {artist.strBiographyEN.substring(0,200)}...
                  {
                    artist.strBiographyEN !== "" ? <a className="modal-trigger" href="#artistBioModal">More</a> : ""
                  }
                </p>
                <h6>Genre : {artist.strGenre !== "" ? artist.strGenre : notAvailableString}</h6>
                <h6>Country : {artist.strCountry !== "" ? artist.strCountry : notAvailableString}</h6>
              </div>
            </div>
            
            <hr />

            <AlbumnsGrid title="Albums" albums={this.props.albums[this.state.albumPage]} />

            <Paginator list={Object.keys(this.props.albums)} gotoPage={this.gotoPage}/>

          </div>
        }
        
        <div id="artistBioModal" className="modal">
          <div className="modal-content">
            {
              artist !== undefined &&
              <div>
                <h4>{artist.strArtist}</h4>
                <p>{artist.strBiographyEN}</p>
              </div>
            }
          </div>
        </div>

      </div>
    )
  }
}

const mapStateToProps = ({artistReducer}) => ({
  artist : artistReducer.artist.artistData,
  albums : artistReducer.artist.albums
})

const mapDispatchToProps = (dispatch) => ({
  FetchArtistData : (data) => dispatch(FetchArtistData(data))
})

Artist.propTypes = {
  artist: PropTypes.object,
  albums : PropTypes.object,
  FetchArtistData : PropTypes.func
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Artist));