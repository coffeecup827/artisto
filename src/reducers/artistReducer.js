import {ArtistActions} from '../actions/artistActions';

const initialState = {
  artistList : null,
  artist : {
    
  }
}

export function artistReducer(state = initialState, action){

  switch (action.type) {

    case ArtistActions.SEARCH_ARTISTS_BY_NAME:
      return Object.assign({},state,{
        ...state,
        artistList : action.data
      });

    case ArtistActions.UPDATE_ARTIST_DATA:
      return Object.assign({},state,{
        ...state,
        artist : {
          artistData : action.data.artistData,
          albums : action.data.albums
        }
      });

    default:
      return state;
  }
}