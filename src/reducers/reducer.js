import { combineReducers } from 'redux';
import { artistReducer } from './artistReducer';
import { trendingReducer } from './trendingReducer';

const reducer = combineReducers({
  artistReducer,
  trendingReducer
});

export default reducer
