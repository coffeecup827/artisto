import {TrendingActions} from '../actions/trendingActions';

const initialState = {
  tredingAlbumns : [],
  tredingSingles : []
}

export function trendingReducer(state = initialState, action){

  switch (action.type) {
    case TrendingActions.UPDATE_TRENDING:
      return Object.assign({},state,{
        tredingAlbumns : action.data.albumns,
        tredingSingles : action.data.singles
      });

    default:
      return state;
  }

}