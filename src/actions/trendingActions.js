import axios from 'axios';
import {URL} from '../helpers/url';

export const TrendingActions = {
  "UPDATE_TRENDING" : "UPDATE_TRENDING"
}

const UpdateTrendingDispatch = (data) => ({
  type : TrendingActions.UPDATE_TRENDING,
  data
});

export const UpdateTrending = (requestParam) => {
  return function(dispatch){

    let cancelToken = {
      cancelToken: requestParam.RequestData.cancelToken
    }

    let requests = [
      axios.get(URL.TrendingAlbumns,cancelToken),
      axios.get(URL.TrendingSingles,cancelToken)
    ];

    axios.all(requests).then(axios.spread((albumns,singles) => {

      /**
       * The api returns albums and singles in desending order, so sort it in ascending order and update state
       */

      let sortedAlbumns = [];

      for (let index = albumns.data.trending.length - 1; index >= 0 ; index--) {
        sortedAlbumns.push(albumns.data.trending[index]);
      }

      let sortedSingles = [];

      for (let index = singles.data.trending.length - 1; index >= 0 ; index--) {
        sortedSingles.push(singles.data.trending[index]);
      }
      
      dispatch(UpdateTrendingDispatch({
        albumns : sortedAlbumns,
        singles : sortedSingles
      }));
      requestParam.RequestSuccessAction();

    })).catch((e) => {
      if(e.message !== "Cancelled")
        requestParam.RequestFailedAction();
    });

  }
}