import axios from 'axios';
import {URL} from '../helpers/url';

export const ArtistActions = {
  "SEARCH_ARTISTS_BY_NAME" : "SEARCH_ARTISTS_BY_NAME",
  "UPDATE_ARTIST_DATA" : "UPDATE_ARTIST_DATA"
}

const UpdateArtistSearchData = (data) => ({
  type : ArtistActions.SEARCH_ARTISTS_BY_NAME,
  data
})

const UpdateArtistData = (data) => ({
  type : ArtistActions.UPDATE_ARTIST_DATA,
  data
})

export const FetchArtistSearchData = (requestParam) => {
  return function(dispatch){

    let cancelToken = {
      cancelToken: requestParam.RequestData.cancelToken
    }

    axios.get(URL.SearchArtist + requestParam.RequestData.artist, cancelToken).then((response) => {
 
      /**
       * If no artist is of the searched name, update the state as null
       */

      if (response.data.artists === null) {
        requestParam.RequestSuccessAction();
        dispatch(UpdateArtistSearchData(null));
        return;
      }

      const artistList = getArtistList(response);
      
      requestParam.RequestSuccessAction();
      dispatch(UpdateArtistSearchData(artistList));
    }).catch((e) => {
      if(e.message !== "Cancelled")
        requestParam.RequestFailedAction();
    });
  }
}

let getArtistList = (response) => {
  const list = response.data.artists;
  
  let paginate = 0;

  /**
   * change albumsPerPage to change artists shown per page
   */
  const artistPerPage = 6;

  let artistList = {};
  
  for (let index = 0; index < list.length; index++) {

    /**
     * Paginate the arists in a object such that the keys become the pages and 
     * each keys value will be array of artists of length artistPerPage
     */

    if ((index)%artistPerPage === 0) {
      paginate++;
    }
    if (artistList[paginate] === undefined) {
      artistList[paginate] = [];
    }

    let artist = list[index];

    artistList[paginate].push({
      idArtist : artist.idArtist,
      strArtist : artist.strArtist,
      strCountry : artist.strCountry,
      strArtistThumb : artist.strArtistThumb,
      strBiographyEN : artist.strBiographyEN,
      strDisbanded : artist.strDisbanded,
      strGenre : artist.strGenre
    });
  }

  return artistList;
}

export const FetchArtistData = (requestParam) => {
  return function(dispatch){

    let cancelToken = {
      cancelToken: requestParam.RequestData.cancelToken
    }

    let requests = [
      axios.get(URL.ArtistData + requestParam.RequestData.artistId,cancelToken),
      axios.get(URL.AtristAlbums + requestParam.RequestData.artistId,cancelToken)
    ];

    axios.all(requests).then(axios.spread((artistResponse,albumsResponse) => {

      const artistData = getArtistData(artistResponse);
      
      const albums = getAlbumList(albumsResponse);

      dispatch(UpdateArtistData({
        artistData,
        albums
      }))

      requestParam.RequestSuccessAction();
    })).catch((e) => {
      if(e.message !== "Cancelled")
        requestParam.RequestFailedAction();
    });

  }
}

let getArtistData = (artistResponse) => {
  const artist = artistResponse.data.artists[0];

  let artistData = {
    idArtist : artist.idArtist,
    strArtist : artist.strArtist,
    strCountry : artist.strCountry,
    strArtistThumb : artist.strArtistThumb,
    strBiographyEN : artist.strBiographyEN,
    strDisbanded : artist.strDisbanded,
    strGenre : artist.strGenre
  }
  return artistData;
}

let getAlbumList = (albumsResponse) => {
  let albumList = albumsResponse.data.album;

  let paginate = 0;

  let albums = {};
  
  /**
   * change albumsPerPage to change albums shown per page
   */
  const albumsPerPage = 12;

  for (let index = 0; index < albumList.length; index++) {

    /**
     * Paginate the arists in a object such that the keys become the pages and 
     * each keys value will be array of artists of length albumsPerPage
     */

    if ((index)%albumsPerPage === 0) {
      paginate++;
    }
    if (albums[paginate] === undefined) {
      albums[paginate] = [];
    }

    let album = albumList[index];
    albums[paginate].push({
      idAlbum : album.idAlbum,
      idTrend : album.idTrend,
      strAlbumThumb : album.strAlbumThumb,
      strAlbum : album.strAlbum
    });
  }

  return albums;
}