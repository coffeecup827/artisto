export const URL = {
  TrendingAlbumns : "https://www.theaudiodb.com/api/v1/json/1/trending.php?country=us&type=itunes&format=albums&country=us&type=itunes&format=albums",
  TrendingSingles : "https://www.theaudiodb.com/api/v1/json/1/trending.php?country=us&type=itunes&format=singles&country=us&type=itunes&format=singles",
  SearchArtist : "https://www.theaudiodb.com/api/v1/json/1/search.php?s=",
  ArtistData : "https://www.theaudiodb.com/api/v1/json/1/artist.php?i=",
  AtristAlbums : "http://www.theaudiodb.com/api/v1/json/1/album.php?i=",
  AlbumTracks : "http://www.theaudiodb.com/api/v1/json/1/track.php?m=",
  Album : "http://www.theaudiodb.com/api/v1/json/1/album.php?m="
}