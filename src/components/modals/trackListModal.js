/*global M*/
import React, { Component } from 'react'
import axios from 'axios';
import {URL} from '../../helpers/url';
import Loader from '../loader';
import ErrorInfo from '../errorInfo';
import PropTypes from 'prop-types';


/**
 * TrackListModal takes albumid as props, on every change of albumid props,
 * it fetches the info and tracklist of the album and shows it.
 * It takes takes albumid as prop
 */
export default class TrackListModal extends Component {

  constructor(props){
    super(props);
    this.state = {
      showError : false,
      showLoader : false,
      showContent : false,
      cancelSource : null,
      tracks : [],
      album : {}
    }
  }

  componentDidMount(){
    M.Modal.init(document.getElementById(this.props.modalid));  
  }

  componentWillReceiveProps(nextProps){
    if(nextProps.albumid !== null){

      /**
       * clear existing data first so as to prevent showing of previos data.
       */

      this.setState({
        showLoader : true,
        showContent : false,
        showError : false,
        tracks : [],
        album : {}
      },() => {
        this.fetchAlbumData(nextProps.albumid);
      })
    }
  }

  fetchAlbumData = (albumId) => {
    /**
     * clear any ongoing requests to prevent race of request
     */
    if (this.state.cancelSource!==null) {
      this.state.cancelSource.cancel("Cancelled");
    }

    let cancelSource = axios.CancelToken.source();

    this.setState({
      cancelSource
    });

    let cancelToken = {
      cancelToken: cancelSource.token
    }

    let requests = [
      axios.get(URL.AlbumTracks + albumId, cancelToken),
      axios.get(URL.Album + albumId, cancelToken)
    ];

    axios.all(requests)
      .then(axios.spread((tracksResponse,albumResponse) => {
        /**
         * structure album and tracks data and update the state
         */
        let albumData = albumResponse.data.album[0];
        
        let album = {
          title : albumData.strAlbum,
          year : albumData.intYearReleased,
          style : albumData.strStyle,
          artist : albumData.strArtist
        }

        let tracks = tracksResponse.data.track.map((track) => ({
          duration : track.intDuration,
          name : track.strTrack,
          id : track.idTrack
        }))

        this.setState({
          album,
          tracks
        },() => {
          this.hideLoader();
        });
      })).catch((e) => {
        if(e.message !== "Cancelled")
          this.showError();
      })
  }


  componentWillUnmount(){
    /**
     * unsubscribe to ongoing requests
     */
    if(this.state.cancelSource != null)
      this.state.cancelSource.cancel("Cancelled");
    M.Modal.getInstance(document.getElementById(this.props.modalid)).close();
  }

  showError = () => {
    this.setState({
      showError : true,
      showLoader : false
    })
  }

  hideLoader = () => {
    this.setState({
      showError : false,
      showLoader : false,
      showContent : true
    })
  }

  render() {

    let notAvailableString  = "Not Available";

    return (
      <div id={this.props.modalid} className="modal">
        <div className="modal-content">
          {
            this.state.showLoader && 
            <Loader />
          }
          {
            this.state.showError && 
            <ErrorInfo />
          }
          {
            this.state.showContent &&
            <div>
              <div className="row">
                <h4>{this.state.album.title}</h4>
                <h6>
                  By : {this.state.album.artist}
                  <span className="right">Style : {this.state.album.style !== "" ? this.state.album.style : notAvailableString}</span>
                </h6>
                <h6>
                  Tracks : {this.state.tracks.length}
                  <span className="right">Year : {this.state.album.year !== 0 ? this.state.album.year : notAvailableString}</span>
                </h6>
              </div>
              <div>
              <ul className="collection">
                {
                  this.state.tracks.map((track) => (
                    <li className="collection-item row" key={track.id}>
                      <p className="col s7 m10"><i className="material-icons blue-text">music_note</i><span className="icon-text">{track.name}</span></p>
                      <p className="col s5 m2"><span className="right"><i className="material-icons blue-text">schedule</i><span className="icon-text"> {track.duration !== 0 ? (track.duration / 60000).toFixed(2) : notAvailableString}m</span></span></p>
                    </li>
                    )
                  )
                }
              </ul>
              </div>
            </div>
          }
        </div>
      </div>
    )
  }
}

TrackListModal.propTypes = {
  modalid: PropTypes.string
};