import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class ErrorInfo extends Component {

  static defaultProps = {
    message: 'Please Try Again'
  }

  render() {
    return (
      <div className="error-info">
        <div className="error-info-content">
          <h6><i className="material-icons red-text error-icon">error_outline</i></h6>
          <h6>{this.props.message}</h6>
        </div>
      </div>
    )
  }
}

ErrorInfo.propTypes = {
  message: PropTypes.string
};