import React, { Component } from 'react'

export default class Loader extends Component {
  render() {
    return (
      <div className="loader">
        <div className="loader-content">
          <div className="preloader-wrapper small active">
            <div className="spinner-layer spinner-green-only">
              <div className="circle-clipper left">
                <div className="circle"></div>
              </div><div className="gap-patch">
                <div className="circle"></div>
              </div><div className="circle-clipper right">
                <div className="circle"></div>
              </div>
            </div>
          </div>
          <h6>Loading...</h6>
        </div>
      </div>
    )
  }
}
