/*global M*/

import React, { Component } from 'react'
import {withRouter} from 'react-router'
import {Link} from 'react-router-dom'

/**
 * Create a navbar with a search bar to search artists.
 * For smaller devices, creates a modal to show search bar
 */

class Navbar extends Component {

  constructor(props){
    super(props);
    this.state = {
      artist : "",
      modalId : "search-modal",
      tapTargetId : "search-info"
    }
  }

  componentDidMount(){
    
    /**
     * Initialise Search Modal and Tap Target(feature discovery) on load
     */

    document.addEventListener('DOMContentLoaded',() => {
      M.Modal.init(document.getElementById(this.state.modalId));  

      M.TapTarget.init(document.getElementById(this.state.tapTargetId)).open();

      this.closeTapTargetOnLargeDevices();

    });
  }

  closeTapTargetOnLargeDevices = () => {
    /**
     * close the feature discovery panel when screen size is of medium device
     */

    if (window.innerWidth > 1200) {
      M.TapTarget.getInstance(document.getElementById(this.state.tapTargetId)).close();
    }

    window.addEventListener("resize", () => {
      if (window.innerWidth > 1200) {
        M.TapTarget.getInstance(document.getElementById(this.state.tapTargetId)).close();
      }
    });
  }

  searchArtist = (e) => {
    e.preventDefault();
    M.Modal.getInstance(document.getElementById(this.state.modalId)).close();
    this.props.history.push("/artists?search=" + this.state.artist);
  }

  updateArtist = (e) => {
    this.setState({
      artist : e.target.value
    })
  }

  handleFabClick = () => {
    M.Modal.getInstance(document.getElementById(this.state.modalId)).open();
    document.getElementById("search-modal-input").focus();
  }

  render() {
    return (
      <div  className="navbar-fixed">

        <nav>
          <div className="nav-wrapper grey lighten-4">

            {
              /**
               * Nav Bar with search option
               */
            }

            <Link to="" className="brand-logo light-blue-text">Artisto</Link>

            <form onSubmit={(e) => this.searchArtist(e)} className="hide-on-med-and-down">
              <div className="input-field navbar-search">
                <input id="artist" value={this.state.artist} onChange={this.updateArtist} type="search" name="artist" autoComplete="off" placeholder="Search Artists" />
                <label className="label-icon" htmlFor="artist"><i className="material-icons grey-text">search</i></label>
              </div>
            </form>

            {
              /**
               * FAB and feature discovery for small devices
               */
            }

            <a 
              id="search-fab" 
              onClick={this.handleFabClick} 
              className="hide-on-large-only search-fab btn-floating btn-large waves-effect waves-light red"
            >
              <i className="material-icons">search</i>
            </a>

            <div id="search-info" className="tap-target hide-on-large-only" data-target="search-fab">
              <div className="tap-target-content">
                <h4>Search</h4>
                <h5>Tap to search artists</h5>
              </div>
            </div>

            {
              /**
               * Modal for small devices
               */
            }

            <div id="search-modal" className="modal bottom-sheet hide-on-large-only">
              <div className="modal-content grey lighten-4">
                <div className="row center no-margin">
                  <div className="col s10 m4 push-m4 push-s1">

                    <form onSubmit={(e) => this.searchArtist(e)} >
                      <div className="input-field col s12">
                        <i className="material-icons prefix">search</i>
                        <input autoFocus="true" id="search-modal-input" value={this.state.artist} onChange={this.updateArtist} type="text" placeholder="Search Artists" />
                      </div>
                    </form>
                    
                  </div>
                </div>
              </div>
            </div>

          </div>
        </nav>

      </div>
    )
  }
}

export default withRouter(Navbar);