/*global M*/
import React, { Component } from 'react';
import TrackListModal from '../modals/trackListModal';
import PropTypes from 'prop-types';


/**
 * Creates a grid to show singles list
 * Takes singles to show as an array and title as string
 * clicking on a single shows info about its album
 */
export default class SinglesGrid extends Component {

  constructor(props){
    super(props);
    this.state = {
      modalId : "single-modal",
      chosenAlbumId : null
    }
  }

  openTrackList = (e) => {
    /**
     *  setting state first before opening the moda so that it 
     * changes the props of TrackListModal, which in turn triggers componentWillReceiveProps of
     * TrackListModal, which clears the state of TrackListModal
     */
    this.setState({
      chosenAlbumId : e.target.getAttribute("albumid")
    },() => {
      M.Modal.getInstance(document.getElementById(this.state.modalId)).open();
    })
  }

  render() {
    return (
      <div>
        <h5 className="grid-header">{this.props.title}</h5>

        <div className="grid-content row">

            {
              this.props.singles.map((single) => (
                <div className="col s10 push-s1 m2 custom-card-col" key={single.idTrend}>
                  
                  <div className="col s12 m12 custom-card">
                    <div onClick={this.openTrackList} albumid={single.idAlbum}  className="custom-card-content z-depth-5 hoverable" 
                      style={{backgroundImage : `url(${(single.strTrackThumb !== null && single.strTrackThumb !== "") ? single.strTrackThumb : require("../../assets/music.png")})`}}
                    > 
                      
                    </div>
                    <div className="custom-card-info">
                      <h6>{single.strTrack}</h6>
                    </div>
                  </div>
                  
                </div>
              ))
            }

        </div>

        <TrackListModal modalid={this.state.modalId} albumid={this.state.chosenAlbumId} />

      </div>
    )
  }
}

SinglesGrid.propTypes = {
  title: PropTypes.string,
  singles : PropTypes.array
};