/*global M*/
import React, { Component } from 'react'
import TrackListModal from '../modals/trackListModal';
import PropTypes from 'prop-types';


/**
 * Creates a grid to show album list
 * Takes albums to show as an array and title as string
 * clicking on a album shows info about the album
 */
export default class AlbumnsGrid extends Component {

  constructor(props){
    super(props);
    this.state = {
      modalId : "album-modal",
      chosenAlbumId : null
    }
  }

  openTrackList = (e) => {
    /**
     *  setting state first before opening the moda so that it 
     * changes the props of TrackListModal, which in turn triggers componentWillReceiveProps of
     * TrackListModal, which clears the state of TrackListModal
     */
    this.setState({
      chosenAlbumId : e.target.getAttribute("albumid")
    },() => {
      M.Modal.getInstance(document.getElementById(this.state.modalId)).open();
    })
  }

  render() {
    return (
      <div>
        <h5 className="grid-header">{this.props.title}</h5>

        <div className="grid-content row">

            {
              this.props.albums.map((album) => (
                <div className="col s10 push-s1 m2 custom-card-col" key={album.idAlbum}>
                  
                  <div className="col s12 m12 custom-card">

                    <div onClick={this.openTrackList} albumid={album.idAlbum} className="custom-card-content z-depth-5 hoverable" 
                      style={{backgroundImage : `url(${(album.strAlbumThumb !== null && album.strAlbumThumb !== "") ? album.strAlbumThumb : require("../../assets/music.png")})`}}
                    >
                      
                    </div>
                    
                    <div className="custom-card-info">
                      <h6>{album.strAlbum}</h6>
                    </div>
                    
                  </div>
                  
                </div>
              ))
            }

            <TrackListModal modalid={this.state.modalId} albumid={this.state.chosenAlbumId} />

        </div>

      </div>
    )
  }
}

AlbumnsGrid.propTypes = {
  title: PropTypes.string,
  albums : PropTypes.array
};