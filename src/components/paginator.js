import React, { Component } from 'react'
import PropTypes from 'prop-types';

/**
 * Creates a Pagination component which takes the page's key as an array and 
 * a prop to trigger a page change in parent. On clicking of a page,
 * it triggers the goto prop and toggles active page
 */

export default class Paginator extends Component {

  constructor(props){
    super(props);
    this.state = {
      showLeft : false,
      showRight : this.props.list.length > 1,
      showHardLeft : false,
      showHardRight : this.props.list.length > 1,
      currentPage : 1,
      isPagesMoreThanOne : this.props.list.length > 1,
      length : this.props.list.length
    }
  }

  componentDidMount(){
    document.getElementById("paginate-1").classList.add("active");
  }

  switchPage = (e) => {
    let selectedPage = e.target.getAttribute("page");
    this.goto(selectedPage);
  }

  gotoPrev = () => {
    if (this.state.currentPage - 1 > 0) {
      this.goto(this.state.currentPage - 1);
    }
  }

  gotoNext = () => {
    if (this.state.currentPage + 1 <= this.state.length) {
      this.goto(this.state.currentPage + 1);
    }
  }

  toggleHelpers = () => {

    /**
     * If current Page is greater than first page, show left helpers
     * If current Page is less than last page, show right helpers
     */

    let showLeftCondition =  (this.state.currentPage > 1 && this.state.isPagesMoreThanOne);

    let showRightCondition = (this.state.currentPage !== this.state.length);
    this.setState({
      showLeft : showLeftCondition,
      showHardLeft : showLeftCondition,
      showRight : showRightCondition,
      showHardRight : showRightCondition
    })
  }

  goto = (selectedPage) => {

    /**
     * Trigger action to go to new page, then toggle the active class
     */

    this.props.gotoPage(selectedPage);
    document.getElementById("paginate-" + this.state.currentPage).classList.remove("active");
    this.setState({
      currentPage : parseInt(selectedPage,10)
    },() => {
      document.getElementById("paginate-" + this.state.currentPage).classList.add("active");
      this.toggleHelpers();
    })
  }

  render() {
    return (
      <div className="row">
        <div className="col s12 m12 center">
          <ul className="pagination">
            <li className={this.state.showHardLeft ? "waves-effect" : "disabled"}><a href="#!" onClick={() => this.goto(1)}><i className="material-icons">first_page</i></a></li>
            <li className={this.state.showLeft ? "waves-effect" : "disabled"}><a href="#!" onClick={() => this.gotoPrev()}><i className="material-icons">chevron_left</i></a></li>
            {
              this.props.list.map((key) => (
                <li className="waves-effect" id={"paginate-"+key} key={key} onClick={this.switchPage}><a page={key} href="#!">{key}</a></li>
              ))
            }
            <li className={this.state.showRight ? "waves-effect" : "disabled"}><a href="#!" onClick={() => this.gotoNext()}><i className="material-icons">chevron_right</i></a></li>
            <li className={this.state.showHardRight ? "waves-effect" : "disabled"}><a href="#!"  onClick={() => this.goto(this.state.length)}><i className="material-icons">last_page</i></a></li>
          </ul>
        </div>
      </div>
    )
  }
}

Paginator.propTypes = {
  list: PropTypes.array,
  gotoPage : PropTypes.func
};